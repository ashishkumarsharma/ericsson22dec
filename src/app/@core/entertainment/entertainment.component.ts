import { Component, OnInit,Renderer2,ElementRef } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { DataService, localService } from 'src/app/services/data.service';
declare var $:any;
import { DomSanitizer } from '@angular/platform-browser';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-entertainment',
  templateUrl: './entertainment.component.html',
  styleUrls: ['./entertainment.component.scss']
})
export class EntertainmentComponent implements OnInit {
  [x: string]: any;
  gameData = [];
  bgImg1;
  bg;
  pointers=[];
  src:any;
  mainurl2;
  constructor(private sanitiser: DomSanitizer,private _ds:DataService, private render: Renderer2, private elementRef: ElementRef, private _auth:AuthService,private _ls:localService) { }

  ngOnInit(): void {
    this.getSettingSection();
    this.getGamezonedata()
  this.getResult()
  }
  pickupLink(data){
   // alert(data)
    
   }
   
   getSettingSection(){
     this._ds.getSettingSection().subscribe(res=>{
       this.bgImg1 = res['bgImages']['entertainment'];
       this.bg=this.sanitiser.bypassSecurityTrustResourceUrl('assets/event/entertainment.jpg');
       this.pointers = res["entertainmentPointers"];
       console.log(this.bgImg)
       console.log(this.pointers)
     });
     // this._auth.settingItems$.subscribe(items => {
     //   if(items.length){
     //     this.bgImg = items[0]['bgImages']['gamezone'];
     //   }
     // });
   }
   getGamezonedata(){
     this._ds.getGamezonedata().subscribe((res:any)=>{
       this.gameData = res.result;
     });
   }
   submitPageAnalytics(item){
    // alert(item)
     this._ls.stepUpAnalytics(item);
   }
   pointerMethod(item,url){
    if(url==="quiz"){
      
     if(this.total<1){
      $("#quiz_modal").modal("show");
    }else{
      Swal.fire({
        position: 'top',
        title: 'You have already played',
        showConfirmButton: false,
        timer: 3000
      });
    } 
    }else{
       $('#open_game_modal').modal('show');
      let user_id = JSON.parse(localStorage.getItem('virtual')).id;
      let name = JSON.parse(localStorage.getItem('virtual')).name;
      
      this.mainurl2 = url+"?user_id="+user_id+"&name="+name
     // alert(this.main_url2)
     this.src=this.sanitiser.bypassSecurityTrustResourceUrl(this.mainurl2);
    }
    
     document.getElementById("popupData").childNodes.forEach((ele:any) => {
     if(ele.id){
       document.getElementById(ele.id).remove();
     }
     }); 
     var iframe:any = document.createElement('iframe');
     iframe.frameBorder=0;
     iframe.width="100%";
     iframe.height="450px";
     iframe.id="randomid";
     iframe.setAttribute("src", item.iframe_url);
     document.getElementById("popupData").appendChild(iframe);
     // const iframe:any = this.render.createElement('iframe');
     // this.render.setAttribute(this.elementRef.nativeElement, 'src', item.iframe_url)
     // this.render.appendChild(this.pdata, iframe);
     // return iframe;  
   }
   closeModal(){
     $('#open_game_modal').modal('hide');
   }
   
   getResult(){
   // alert('ff')
    let data = JSON.parse(localStorage.getItem('virtual'));
    this.user_id=data.id
    this._ds.getQuizResult(this.user_id).subscribe((res:any)=>{
     // alert(res)
     
     this.correct=res.result.correct
    // alert(this.correct)
     this.total=res.result.summary.length

    console.log(this.correct)
    console.log(this.total)
      console.log(res)
    })
  } 
}
