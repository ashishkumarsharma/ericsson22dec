import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EntertainmentRoutingModule } from './entertainment-routing.module';
import { EntertainmentComponent } from './entertainment.component';
import { QuizModule } from 'src/app/@main/shared/quiz/quiz.module';


@NgModule({
  declarations: [EntertainmentComponent],
  imports: [
    CommonModule,
    EntertainmentRoutingModule,
    QuizModule
  ]
})
export class EntertainmentModule { 


}
