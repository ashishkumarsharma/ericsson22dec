import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StageAuditoriumRoutingModule } from './stage-auditorium-routing.module';
import { StageAuditoriumComponent } from './stage-auditorium.component';
import { AskQuestionModule } from 'src/app/@main/shared/ask-question/ask-question.module';
import { AudiMenuModule } from '../audi-menu/audi-menu.module';
import { PollModule } from 'src/app/@main/shared/poll/poll.module';
import { GroupChatModule } from 'src/app/@main/shared/group-chat/group-chat.module';
import { QuizModule } from 'src/app/@main/shared/quiz/quiz.module';


@NgModule({
  declarations: [StageAuditoriumComponent],
  imports: [
    CommonModule,
    StageAuditoriumRoutingModule,
    AskQuestionModule,
    AudiMenuModule,
    PollModule,
    GroupChatModule,
    QuizModule
  ]
})
export class StageAuditoriumModule { }
