import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { DataService, localService } from 'src/app/services/data.service';
declare var $:any;
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-game-zone',
  templateUrl: './game-zone.component.html',
  styleUrls: ['./game-zone.component.scss']
})
export class GameZoneComponent implements OnInit {
  [x: string]: any;
  gameData = [];
  bgImg;
  pointers=[];
  src:any;
  constructor(private _ls:localService ,private sanitiser: DomSanitizer,private _ds:DataService, private render: Renderer2, private elementRef: ElementRef, private _auth:AuthService) { }


  ngOnInit(): void {
    this.getSettingSection();
    this.getGamezonedata();
  }

  
  
  getSettingSection(){
    this._ds.getSettingSection().subscribe(res=>{
      this.bgImg = res['bgImages']['gamezone'];
      this.pointers = res["gamezonePointers"];
      console.log(this.bgImg)
      console.log(this.pointers)
    });
    // this._auth.settingItems$.subscribe(items => {
    //   if(items.length){
    //     this.bgImg = items[0]['bgImages']['gamezone'];
    //   }
    // });
  }
 /*  getGamezonedata(){
    this._ds.getGamezonedata().subscribe((res:any)=>{
      this.gameData = res.result;
    });
  } */
  pointerMethod(item,data){
    if(data=='https://engagements.skunkworksxp.in/XYZ/maze/'){
  /* $('#open_game_modal1').modal('show'); */
  let user_id = JSON.parse(localStorage.getItem('virtual')).id;
  let name = JSON.parse(localStorage.getItem('virtual')).name;
  this.mainurl2 = data+"index.html?user_id="+user_id+"&name="+name
  window.open(this.mainurl2,"_blank")

  /* this.src=this.sanitiser.bypassSecurityTrustResourceUrl(this.mainurl2); */
 }else{
    $('#open_game_modal').modal('show');
    let user_id = JSON.parse(localStorage.getItem('virtual')).id;
    let name = JSON.parse(localStorage.getItem('virtual')).name;
    
    this.mainurl2 = data+"?user_id="+user_id+"&name="+name
    this.src=this.sanitiser.bypassSecurityTrustResourceUrl(this.mainurl2);
 }
    document.getElementById("popupData").childNodes.forEach((ele:any) => {
    if(ele.id){
      document.getElementById(ele.id).remove();
    }
    }); 
    var iframe:any = document.createElement('iframe');
    iframe.frameBorder=0;
    iframe.width="100%";
    iframe.height="450px";
    iframe.id="randomid";
    iframe.setAttribute("src", item.iframe_url);
    document.getElementById("popupData").appendChild(iframe);
    // const iframe:any = this.render.createElement('iframe');
    // this.render.setAttribute(this.elementRef.nativeElement, 'src', item.iframe_url)
    // this.render.appendChild(this.pdata, iframe);
    // return iframe;  
  }
   
  submitPageAnalytics(item){
   // alert(item)
    this._ls.stepUpAnalytics(item);
  }
  closeModal(){
    $('#open_game_modal').modal('hide');
  }
  closeModal1(){
    $('#open_game_modal1').modal('hide');
  }
}
