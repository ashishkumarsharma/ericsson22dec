import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ChatService } from 'src/app/services/chat.service';
import { DataService } from 'src/app/services/data.service';
declare var $:any;
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-poll',
  templateUrl: './poll.component.html',
  styleUrls: ['./poll.component.scss']
})
export class PollComponent implements OnInit {
  pollingList:any =[];
  poll_id;
  showmsg=true;
  showPoll = false;
  pollForm = new FormGroup({
    polling: new FormControl(''),
  });
  msg;
  audi_id;
  constructor(private _ds : DataService,private chat : ChatService) { }
  
  ngOnInit(): void {
    this.chat.getSocketMessages().subscribe(((data:any)=>{
      console.log(data)
      let poll = data;
      let polls = poll.split('_');
      if (polls[0] == 'start' && polls[1]=='poll'){
       
          $('#poll_modal').modal('show')
     
        this.poll_id = polls[2];
        this.audi_id = polls[3];
        this.getPollsList()        
      }
    }));
  }
  closeModal() {
    $('#poll_modal').modal('hide');
  }
  getPollsList() {
    this._ds.getPollsList(this.poll_id).subscribe((res:any)=>{
      this.pollingList = res.result;
    })
  }
  pollSubmit(id){
    let data = JSON.parse(localStorage.getItem('virtual'));    
    this._ds.pollSubmit(id,data.id,this.pollForm.value.polling).subscribe(res=>{
      if(res.code == 1){
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Thanks for submitting Poll',
          showConfirmButton: false,
          timer: 3000
        });       
        $('#poll_modal').modal('hide');      
      }
      this.pollingList = [];
    });
  }

}

