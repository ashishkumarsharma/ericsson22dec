export const MenuItems = [
    {icon:'assets/icons/01.png', path:'lobby', name:'lobby'},
    {icon:'assets/icons/02.png', path:'auditorium', name:'auditorium'},
    {icon:'assets/icons/03.png', path:'exhibition/home/1', name:'exhibition1'},
    {icon:'assets/icons/03.png', path:'exhibition/home/2', name:'exhibition2'},
    {icon:'assets/icons/04.png', path:'lounge', name:'lounge'},
    {icon:'assets/icons/05.png', path:'gamezone', name:'game zone'},
    {icon:'assets/icons/06.png', path:'photobooth', name:'photobooth'},
];