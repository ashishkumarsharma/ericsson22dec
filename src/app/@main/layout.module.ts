import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { MenuComponent } from './menu/menu.component';
import { RouterModule } from '@angular/router';
import { BriefcaseModalComponent } from './briefcase-modal/briefcase-modal.component';
import { LeaderboardModalComponent } from './leaderboard-modal/leaderboard-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [LayoutComponent, MenuComponent, BriefcaseModalComponent, LeaderboardModalComponent],
  imports: [
    CommonModule,
    RouterModule,FormsModule,
    LayoutRoutingModule,ReactiveFormsModule
  ]
})
export class LayoutModule { }
